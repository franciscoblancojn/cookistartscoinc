const app = {
    request : (type, resultF, json = {}) => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
    
        var raw = {
            "type":     type,
            "user":     localStorage.user,
            "password": localStorage.password,
            "host":     localStorage.host,
        };
        for (const key in json) {
            raw[key] = json[key]
        }
        raw = JSON.stringify(raw)
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        var url = "https://pixeltracking.startscoinc.com/app";
        fetch(url, requestOptions)
            .then(response => response.text())
            .then(result => {
                result = JSON.parse(result)
                
                localStorage.setItem( "type" , result.type )
                if (result.type == "error") {
                    alert(result.message)
                } else {
                    resultF(result)
                }
            })
            .catch(error => console.log('error', error));
    }
}


export default app;