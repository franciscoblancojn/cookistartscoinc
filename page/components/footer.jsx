const Footer = (pros) => {
    return (
        <footer>
            <p>
                All rights reserved 2021 - THEADGROUP |  StartsCo - UppConsulting - MediaLop - Almma Studio
            </p>
            <div className="btnTheme" onClick={()=>{
                localStorage.setItem('themeDarck',!(localStorage.themeDarck=="true"))
                document.body.classList.toggle('themeDarck')
            }}></div>
        </footer>
    )
}

export default Footer