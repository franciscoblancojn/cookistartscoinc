import Head from 'next/head'
import Footer from './footer'

const content = (pros) => {
    
    return (
        <div className="content">
            <Head>
                <title>{pros.title}</title>
            </Head>
            <div id="page">
                {pros.children}
            </div>
            <Footer></Footer>
        </div>
    )
}

export default content