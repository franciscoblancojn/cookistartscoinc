import React, { Component } from "react"
import Table from "./table";



class TableV extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            json : props.json,
        }
    }
    render(){
        const json = this.state.json
        if(json == undefined){
            return(
                <h2>
                    No Item
                </h2>
            );
        }
        const keys = Object.keys(json)
        return(
            <table>
                <tbody>
                {keys.map((key,i)=>{
                    var value = json[key]
                    if(Array.isArray(value)){
                        value = (<Table rows={value}></Table>)
                    }else if(typeof value == "object"){
                        value = (<Table rows={[value]}></Table>)
                        // value = (<TableV json={value}></TableV>)
                    }
                    return (
                        <tr key={i}>
                            <th>{key}</th>
                            <td>{value}</td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
        )
    }
}
export default TableV