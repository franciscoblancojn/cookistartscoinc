import React, { Component } from "react"
import Link from 'next/link'
import Filters from "./filters";

const printTh = (e) => {
    switch (e) {
        case "ipCustomer":
            return "ip"
            break;
    }
    return e
}
const printTd = (key , e , page = "") => {
    switch (key) {
        case "Browser":
            return e.split("<hr>").map((e)=>(
                <div className="contentBrowser">
                    <img src={`/icons/${e.split(" ").join('_')}.png`} alt={e}/>
                    {e}
                </div>
            ))
        case "OS":
            return e.split("<hr>").map((e)=>(
                <div className="contentOS">
                    <img src={`/icons/${e.split(" ").join('_')}.png`} alt={e}/>
                    {e}
                </div>
            ))
        case "Pais":
            return e.split("<hr>").map((e)=>(
                <div className="contentOS">
                    <img src={`/icons/${e.split(" ").join('_')}.png`} alt={e}/>
                    {e}
                </div>
            ))
        case "id" && typeof e == "string":
            return `${e.slice(0,10)}${(e.length>10)?"...":""}`
        default:
            return e
    }
}


class Table extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pageUrl : props.page,
            rows : props.rows,
            rowsFilter : props.rows,
            rowsGlobal : props.rows,
            orderby : "",
            order : "none",
            isFilter : false,
            pagination:10,
            page:1
        }
        this.changePagination = this.changePagination.bind(this);
        this.nextPage = this.nextPage.bind(this);
        this.prevPage = this.prevPage.bind(this);
        this.orderBy = this.orderBy.bind(this);
        this.changeRowFilters = this.changeRowFilters.bind(this);
        this.clearFilter = this.clearFilter.bind(this);
    
    }
    changePagination = (e) =>{
        const value = e.target.value
        this.setState({
            pagination:value,
            page:1
        })
    }
    prevPage = () => {
        if(this.state.page>1){
            this.setState({page:--this.state.page})
        }
    }
    nextPage = () => {
        if(this.state.page<Math.ceil(this.state.rows.length/this.state.pagination)){
            this.setState({page:++this.state.page})
        }
    }
    changeRowFilters(newRows){
        this.setState({
            rowsFilter:newRows,
            rows:newRows,
            isFilter:true,
        })
    }
    orderBy(key){
        this.setState({page:1})
        var order = "top"
        const auxrows = [...this.state.rowsFilter]
        if(key == this.state.orderby){
            switch (this.state.order) {
                case "top":
                    this.setState({order : "down"})
                    order="down"
                    break;
                case "down":
                    this.setState({order : "none"})
                    order="none"
                    break;

                case "none":
                    this.setState({order : "top"})
                    break;
            }
        }else{
            this.setState({
                orderby : key,
                order : "top"
            })
        }
        switch (order) {
            case "top":
                this.setState({
                    rows : auxrows.sort(function (a, b) {
                        var swObj = false;
                        if(typeof  a[key] == "object"){
                            a[key] = JSON.stringify(a[key])
                            b[key] = JSON.stringify(b[key])
                            swObj = true;
                        }else if(typeof  a[key] != "string"){
                            a[key] = a[key].toString()
                            b[key] = b[key].toString()
                        }
                        const sw = b[key].localeCompare(a[key])
                        if(swObj){
                            a[key] = JSON.parse(a[key])
                            b[key] = JSON.parse(b[key])
                        }
                        return sw;
                    })
                })
                break;
            case "down":
                this.setState({
                    rows : auxrows.sort(function (a, b) {
                        var swObj = false;
                        if(typeof  a[key] == "object"){
                            a[key] = JSON.stringify(a[key])
                            b[key] = JSON.stringify(b[key])
                            swObj = true;
                        }else if(typeof  a[key] != "string"){
                            a[key] = a[key].toString()
                            b[key] = b[key].toString()
                        }
                        const sw = a[key].localeCompare(b[key])
                        if(swObj){
                            a[key] = JSON.parse(a[key])
                            b[key] = JSON.parse(b[key])
                        }
                        return sw;
                    })
                })
                break;

            case "none":
                this.setState({
                    rows : this.state.rowsFilter
                })
                break;
        }
    }
    clearFilter(){
        this.setState({
            rowsFilter:this.state.rowsGlobal,
            rows:this.state.rowsGlobal,
            isFilter:false,
        })
    }
    render(){
        const btnFilter = (this.state.isFilter)?(
            <a className="btnHeader" onClick={this.clearFilter}>Clear Filter</a>
        ):""

        var rows = this.state.rows
        if(rows == undefined){
            return(
                <div>
                    <h2>Error Rows Invalid</h2>
                    {btnFilter}
                </div>
            )
        }
        if(rows.length == 0){
            return(
                <div>
                    <h2>No Items</h2>
                    {btnFilter}
                </div>
            )
        }
        const thead = Object.keys(rows[0])
        return  (
            <div>
                <div>
                    {btnFilter}
                </div>
                <br/>
                {(this.props.noneFilter == undefined)?(
                    <Filters
                    keys={thead}
                    rows={this.state.rowsGlobal}
                    changeRowFilters={this.changeRowFilters}
                    ></Filters>
                ):""}
                <table>
                    <thead>
                        <tr>
                            {(this.props.page!=undefined)?(<th></th>):""}
                            {thead.map((e,i)=>(
                                <th value={e} key={i} orderby={`${(e==this.state.orderby)}`} order={this.state.order} onClick={()=>this.orderBy(e)}>
                                    <span className="th">
                                        {printTh(e)}
                                        <span className="arrow orderby">
                                            <svg width="17" height="10" viewBox="0 0 17 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.22505 9.23709C9.13163 9.33543 9.01918 9.41373 8.89454 9.46724C8.7699 9.52075 8.63568 9.54834 8.50004 9.54834C8.36441 9.54834 8.23019 9.52075 8.10555 9.46724C7.98091 9.41373 7.86846 9.33543 7.77504 9.23709L0.600046 1.68909C0.465008 1.54704 0.374685 1.36845 0.340305 1.1755C0.305925 0.982546 0.329002 0.783745 0.40667 0.6038C0.484337 0.423855 0.613169 0.2707 0.777156 0.163367C0.941144 0.0560341 1.13306 -0.000743866 1.32905 8.86917e-05L15.671 8.86917e-05C15.867 -0.000743866 16.0589 0.0560341 16.2229 0.163367C16.3869 0.2707 16.5158 0.423855 16.5934 0.6038C16.6711 0.783745 16.6942 0.982546 16.6598 1.1755C16.6254 1.36845 16.5351 1.54704 16.4 1.68909L9.22505 9.23709Z" fill="var(--color-4)"/>
                                            </svg>
                                        </span>
                                    </span>
                                </th>
                            ))}
                        </tr>
                    </thead>
                    <tbody>
                        {rows.slice((this.state.page-1)*this.state.pagination,(this.state.page)*this.state.pagination).map((e,i)=>(
                            <tr key={i}>

                                {(this.props.page!=undefined)?(<td>
                                    <Link href={`/${this.state.pageUrl}/${e.id}`}>
                                        <a className="iconSingle">
                                            <svg width="23" height="16" viewBox="0 0 23 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M11.205 4.58398C10.6006 4.58398 10.0097 4.7632 9.50716 5.099C9.00461 5.4348 8.61291 5.91208 8.38161 6.47049C8.15031 7.0289 8.08979 7.64337 8.20771 8.23618C8.32562 8.82898 8.61668 9.3735 9.04407 9.80089C9.47146 10.2283 10.016 10.5193 10.6088 10.6373C11.2016 10.7552 11.8161 10.6946 12.3745 10.4633C12.9329 10.232 13.4102 9.84036 13.746 9.3378C14.0818 8.83524 14.261 8.2444 14.261 7.63998C14.2605 6.82965 13.9383 6.05264 13.3653 5.47964C12.7923 4.90665 12.0153 4.58451 11.205 4.58398Z" fill="var(--color-4)"/>
                                                <path d="M11.2 8.43178e-08C8.78204 0.00240565 6.42075 0.732369 4.42326 2.09494C2.42577 3.45751 0.884532 5.38963 0 7.64C0.883753 9.89187 2.42551 11.8252 4.42423 13.188C6.42296 14.5507 8.78592 15.2796 11.205 15.2796C13.6241 15.2796 15.987 14.5507 17.9858 13.188C19.9845 11.8252 21.5262 9.89187 22.41 7.64C21.5254 5.38765 19.9828 3.45404 17.9832 2.09126C15.9836 0.728466 13.6198 -0.000286203 11.2 8.43178e-08ZM11.2 12.733C10.193 12.7316 9.20901 12.4317 8.37239 11.8713C7.53577 11.3108 6.88408 10.5149 6.49968 9.58416C6.11527 8.65341 6.01541 7.6296 6.21272 6.64212C6.41002 5.65464 6.89563 4.74781 7.60818 4.03624C8.32073 3.32467 9.22822 2.8403 10.216 2.64436C11.2037 2.44841 12.2274 2.54967 13.1576 2.93536C14.0878 3.32104 14.8828 3.97382 15.4422 4.81121C16.0015 5.6486 16.3 6.633 16.3 7.64C16.2997 8.30934 16.1676 8.97207 15.9111 9.59032C15.6546 10.2086 15.2789 10.7702 14.8052 11.2432C14.3316 11.7162 13.7694 12.0912 13.1508 12.3468C12.5322 12.6024 11.8693 12.7337 11.2 12.733Z" fill="var(--color-4)"/>
                                            </svg>
                                        </a>
                                    </Link>
                                </td>):""}
                                
                                {thead.map((ele,j)=>{
                                    var value = e[ele]
                                    if(Array.isArray(value)){
                                        // value = JSON.stringify(value)
                                        value = (<Table rows={value} noneFilter="1"></Table>)
                                    }else if(typeof value == "object" && value!=null){
                                        // value = JSON.stringify(value)
                                        value = (<Table rows={[value]} noneFilter="1"></Table>)
                                    }
                                    return (
                                        <td key={j}>
                                            {printTd(ele,value,this.state.page)}
                                        </td>
                                    )
                                })}
                            </tr>
                        ))}
                    </tbody>
                </table>
                <div className="Pagination">
                    <div className="contentMostrar">
                        Mostrar 
                        <input type="number" id="mostrar" name="mostrar" defaultValue={this.state.pagination} onChange={this.changePagination} />
                    </div>
                    <div className="contentPage">
                        Paginas 
                        <span>
                        {this.state.page} - {Math.ceil(rows.length/this.state.pagination)}
                        </span>
                        <button className="btnPagination Left" onClick={this.prevPage} isDisable={`${this.state.page==1}`}>
                            <svg width="14" height="19" viewBox="0 0 14 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0.849004 10.6459C0.721534 10.5532 0.617809 10.4316 0.546294 10.291C0.474779 10.1505 0.4375 9.9951 0.4375 9.83744C0.4375 9.67978 0.474779 9.52435 0.546294 9.38384C0.617809 9.24333 0.721534 9.12173 0.849004 9.02894L11.675 1.15594C11.8242 1.04748 12.0005 0.982403 12.1844 0.967899C12.3683 0.953396 12.5526 0.990038 12.7169 1.07377C12.8813 1.1575 13.0193 1.28506 13.1157 1.44234C13.212 1.59962 13.263 1.78049 13.263 1.96494L13.263 17.7099C13.263 17.8944 13.212 18.0753 13.1157 18.2325C13.0193 18.3898 12.8813 18.5174 12.7169 18.6011C12.5526 18.6848 12.3683 18.7215 12.1844 18.707C12.0005 18.6925 11.8242 18.6274 11.675 18.5189L0.849004 10.6459Z" fill="var(--color-1)"/>
                            </svg>
                        </button>
                        <button className="btnPagination Right" onClick={this.nextPage} isDisable={`${this.state.page==Math.ceil(this.state.rows.length/this.state.pagination)}`}>
                            <svg width="14" height="19" viewBox="0 0 14 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0.849004 10.6459C0.721534 10.5532 0.617809 10.4316 0.546294 10.291C0.474779 10.1505 0.4375 9.9951 0.4375 9.83744C0.4375 9.67978 0.474779 9.52435 0.546294 9.38384C0.617809 9.24333 0.721534 9.12173 0.849004 9.02894L11.675 1.15594C11.8242 1.04748 12.0005 0.982403 12.1844 0.967899C12.3683 0.953396 12.5526 0.990038 12.7169 1.07377C12.8813 1.1575 13.0193 1.28506 13.1157 1.44234C13.212 1.59962 13.263 1.78049 13.263 1.96494L13.263 17.7099C13.263 17.8944 13.212 18.0753 13.1157 18.2325C13.0193 18.3898 12.8813 18.5174 12.7169 18.6011C12.5526 18.6848 12.3683 18.7215 12.1844 18.707C12.0005 18.6925 11.8242 18.6274 11.675 18.5189L0.849004 10.6459Z" fill="var(--color-1)"/>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}
export default Table