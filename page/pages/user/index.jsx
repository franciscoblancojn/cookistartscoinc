import React, { Component } from "react"
import ls from 'local-storage'

import Content from "@/components/content";
import Login from "@/components/login";
import Panel from "@/components/panel";
import App from "@/components/app";
import Loader from "@/components/loader";
import Table from "@/components/table";

class User extends React.Component {
    state = {
        content:(<Loader/>),
        user:[]
    }
    componentDidMount(){
        if(ls.get('type') == "ok"){
            App.request("getUsers",(result) => {
                var result = result.result
                this.setState({user:result})
                this.setState({
                    content:(<div>
                        <Panel/>
                        <Table rows={this.state.user} page="user"></Table>
                    </div>)
                })
            })
        }else{
            this.setState({
                content:(<Login/>)
            })
        }
    }
    render() {
        return (
            <Content title="Pixeltracking">
                {this.state.content}
            </Content>
        )
    }
}
export default User