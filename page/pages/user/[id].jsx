import React, { Component } from "react"
import ls from 'local-storage'

import Content from "@/components/content";
import Login from "@/components/login";
import Panel from "@/components/panel";
import App from "@/components/app";
import Loader from "@/components/loader";
import TableV from "@/components/tableV";
import Table from "@/components/table";

class User extends React.Component {
    state = {
        content:(<Loader/>),
        user:{}
    }
    static async getInitialProps({ query }) {
        const id = query.id
        return {
            id:id
        }
    }
    componentDidMount(){
        if(ls.get('type') == "ok"){
            App.request("getCustomer",(result) => {
                var result = result
                this.setState({user:result})
                const customer = result.customer
                const events = result.events
                const orders = result.orders
                const addToCart = result.addToCart
                
                this.setState({
                    content:(<div>
                        <Panel/>
                        <div className="contentInfoLeft">
                            <div className="colLeft">
                                <ul className="menuSticky">
                                    <li className="liMenuSticky">
                                        <a href="#customer">Customer</a>
                                    </li>
                                    <li className="liMenuSticky">
                                        <a href="#events">Eventos</a>
                                    </li>
                                    <li className="liMenuSticky">
                                        <a href="#orders">Ventas</a>
                                    </li>
                                    <li className="liMenuSticky">
                                        <a href="#addToCart">Agregados al Carrito</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="colRight">
                                <div id="customer">
                                    <h1 className="titleContentStikit">Customer</h1>
                                    <TableV json={customer}></TableV>
                                </div>
                                <div id="events">
                                    <h1 className="titleContentStikit">Eventos</h1>
                                    <Table rows={events} page="event"></Table>
                                </div>
                                <div id="orders">
                                    <h1 className="titleContentStikit">Ventas</h1>
                                    <Table rows={orders} page="order"></Table>
                                </div>
                                <div id="addToCart">
                                    <h1 className="titleContentStikit">Agregados al Carrito</h1>
                                    <Table rows={addToCart} page="product"></Table>
                                </div>
                            </div>
                        </div>
                    </div>)
                })
            },{
                "idCustomer":this.props.id
            })
        }else{
            this.setState({
                content:(<Login/>)
            })
        }
    }
    render() {
        return (
            <Content title="Pixeltracking">
                {this.state.content}
            </Content>
        )
    }
}
export default User