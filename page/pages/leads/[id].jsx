import React, { Component } from "react"
import ls from 'local-storage'

import Content from "@/components/content";
import Login from "@/components/login";
import Panel from "@/components/panel";
import App from "@/components/app";
import Loader from "@/components/loader";
import Table from "@/components/table";
import TableV from "@/components/tableV";

class Event extends React.Component {
    state = {
        content:(<Loader/>),
        event:[]
    }
    static async getInitialProps({ query }) {
        const id = query.id
        return {
            id:id
        }
    }
    componentDidMount(){
        if(ls.get('type') == "ok"){
            App.request("getEvent",(result) => {
                var result = result.result
                this.setState({event:result})
                this.setState({
                    content:(<div>
                        <Panel/>
                        <TableV json={this.state.event}></TableV>
                    </div>)
                })
            },
            {
                id:this.props.id
            })
        }else{
            this.setState({
                content:(<Login/>)
            })
        }
    }
    render() {
        return (
            <Content title="Pixeltracking">
                {this.state.content}
            </Content>
        )
    }
}
export default Event