import React, { Component } from "react"
import ls from 'local-storage'

import Content from "@/components/content";
import Login from "@/components/login";
import Panel from "@/components/panel";
import App from "@/components/app";
import Loader from "@/components/loader";
import TableV from "@/components/tableV";
import Table from "@/components/table";

class Product extends React.Component {
    state = {
        content:(<Loader/>),
        product:{}
    }
    static async getInitialProps({ query }) {
        const id = query.id
        return {
            id:id
        }
    }
    componentDidMount(){
        if(ls.get('type') == "ok"){
            App.request("getSingleProduct",(result) => {
                var result = result.result
                this.setState({product:result})
                this.setState({
                    content:(<div>
                        <Panel/>
                        <h1>Producto</h1>
                        <TableV json={this.state.product}></TableV>
                    </div>)
                })
            },{
                "id":this.props.id
            })
        }else{
            this.setState({
                content:(<Login/>)
            })
        }
    }
    render() {
        return (
            <Content title="Pixeltracking">
                {this.state.content}
            </Content>
        )
    }
}
export default Product