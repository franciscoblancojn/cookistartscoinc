import React, { Component } from "react"
import ls from 'local-storage'

import Content from "@/components/content";
import Login from "@/components/login";
import Panel from "@/components/panel";
import App from "@/components/app";
import Loader from "@/components/loader";
import Table from "@/components/table";

class Event extends React.Component {
    state = {
        content:(<Loader/>),
        event:[]
    }
    componentDidMount(){
        if(ls.get('type') == "ok"){
            App.request("getProduct",(result) => {
                var result = result.result
                this.setState({event:result});
                this.setState({
                    content:(<div>
                        <Panel/>
                        <Table rows={this.state.event} page="leads/agregados_al_carrito"></Table>
                    </div>)
                })
            })
        }else{
            this.setState({
                content:(<Login/>)
            })
        }
    }
    render() {
        return (
            <Content title="Pixeltracking">
                {this.state.content}
            </Content>
        )
    }
}
export default Event