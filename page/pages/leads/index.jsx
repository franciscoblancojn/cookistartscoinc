import React, { Component } from "react"
import ls from 'local-storage'

import Content from "@/components/content";
import Login from "@/components/login";
import Panel from "@/components/panel";
import App from "@/components/app";
import Loader from "@/components/loader";
import Table from "@/components/table";

class Event extends React.Component {
    state = {
        content:(<Loader/>),
        event:[]
    }
    componentDidMount(){
        if(ls.get('type') == "ok"){
            App.request("getData",(result) => {
                var r = result.result;
                this.setState({event:r});
                this.setState({
                    content:(<div>
                        <Panel/>
                        <Table rows={this.state.event} page="leads"></Table>
                    </div>)
                })
            },
            {
                return:[
                    "platform"
                ],
                query:[
                    {
                        "key":"event",
                        "value":"click",
                        "compare":"="
                    }
                ]
            })
        }else{
            this.setState({
                content:(<Login/>)
            })
        }
    }
    render() {
        return (
            <Content title="Pixeltracking">
                {this.state.content}
            </Content>
        )
    }
}
export default Event