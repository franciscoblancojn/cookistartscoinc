import React, { Component } from "react"
import ls from 'local-storage'

import Content from "@/components/content";
import Login from "@/components/login";
import Panel from "@/components/panel";
import SelectCuenta from "@/components/selectCuenta";
import UserActive from "@/components/userActive";
import InfoCuenta from "@/components/infoCuenta";

class Index extends React.Component {
    state = {
        content:(<Login/>)
    }
    componentDidMount(){
        if(ls.get('type') == "ok"){
            this.setState({
                content:(<div>
                    <Panel/>
                    <h1 className="titlePP">
                        Cuenta
                    </h1>
                    <SelectCuenta></SelectCuenta>
                    <div className="row">
                        <div className="col-6">
                            <InfoCuenta></InfoCuenta>
                        </div>
                        <div className="col-6">
                            <UserActive></UserActive>
                        </div>
                    </div>
                </div>)
            })
        }
    }
    render() {
        return (
            <Content title="Pixeltracking">
                {this.state.content}
            </Content>
        )
    }
}
export default Index