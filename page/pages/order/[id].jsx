import React, { Component } from "react"
import ls from 'local-storage'

import Content from "../../components/content";
import Login from "../../components/login";
import Panel from "../../components/panel";
import App from "../../components/app";
import Loader from "../../components/loader";
import TableV from "../../components/tableV";
import Table from "../../components/table";

class Order extends React.Component {
    state = {
        content:(<Loader/>),
        order:{}
    }
    static async getInitialProps({ query }) {
        const id = query.id
        return {
            id:id
        }
    }
    componentDidMount(){
        if(ls.get('type') == "ok"){
            App.request("getOrder",(result) => {
                var result = result.result
                this.setState({order:result})
                var order = this.state.order
                const products = order.products
                delete order.products;
                this.setState({
                    content:(<div>
                        <Panel/>
                        <h1>User</h1>
                        <TableV json={order}></TableV>
                        <h1>Productos</h1>
                        <Table rows={products} page="product"></Table>
                    </div>)
                })
            },{
                "id":this.props.id
            })
        }else{
            this.setState({
                content:(<Login/>)
            })
        }
    }
    render() {
        return (
            <Content title="Pixeltracking">
                {this.state.content}
            </Content>
        )
    }
}
export default Order