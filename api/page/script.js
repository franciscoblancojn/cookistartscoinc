
app =  document.getElementById('app')

var rowTbodyGloabal,rowTbodyFilters,selectCuenta,cuentas;
var keysNumber = [
    "product_id","quantity","variation_id","price","weight","length","width","height"
]
var keysRange = [
    "id",
    "product_id","order_id","customer_id","Sessions","Events",
    "date",
    "quantity",
    "variation_id",
    "price",
    "sku",
    "weight",
    "length",
    "width",
    "height",
    "total",
    "time"
]
var keysSelect = [
    "product_type",
    "state",
    "payment",
    "platform",
    "event",
    "OS",
    "Browser",
    "System"
]
var keysSearch = [
    "name",
    "ip",
    "billing_first_name",	
    "billing_last_name",
    "billing_address_1",
    "billing_address_2",
    "billing_city",
    "billing_state",
    "billing_postcode",
    "billing_country",
    "billing_email",
    "billing_phone",
    "products",
    "event_target",
    "event_id",
    "event_class",
    "utm",
    "ipCustomer"
]
var keysNone = [
    "permalink",
    "host",
    "url",
    "userAgent",
    "pageUrl",
    "event_href"
]
request = (type, resultF, json = {}) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = {
        "type":     type,
        "user":     localStorage.user,
        "password": localStorage.password,
        "host":     localStorage.host,
    };
    for (const key in json) {
        raw[key] = json[key]
    }
    raw = JSON.stringify(raw)
    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
    };
    var url = "https://pixeltracking.startscoinc.com/app";
    fetch(url, requestOptions)
        .then(response => response.text())
        .then(result => {
            result = JSON.parse(result)
            
            localStorage.setItem( "type" , result.type )
            if (result.type == "error") {
                alert(result.message)
            } else {
                resultF(result)
            }
        })
        .catch(error => console.log('error', error));
}

printValue = (value) => {
    if(typeof value == "string"){
        if (value.search("http")==0) {
            value = `
                <a href="${value}" target="_blank">
                    ${value}
                </a>
            `
        }else if(value.search("www.")==0){
            value = `
                <a href="${value}" target="_blank">
                    ${value}
                </a>
            `
        }
    }
    return value;
}

printJson = (json) => {
    html = ""
    for (const key in json) {
        value = json[key]
        html += `
            <tr>
                <td>${key}</td>
                <td>${printValue(value)}</td>
            </tr>
        `
    }
    return `<table>${html}</table>`
}
printArray = (array_element) => {
    var html = ""
    for (i_a = 0; i_a < array_element.length; i_a++) {
        value = array_element[i_a];
        value = printJson(value)
        html += `
            <tr>
                <td>${i_a}</td>
                <td>${printValue(value)}</td>
            </tr>
        `
    }
    return `<table>${html}</table>`
}
printTbody = (rows) => {
    var keys = Object.keys(rows[0])
    var rowsHTML = ""
    var rowHTML = ""
    for (i = 0; i < rows.length; i++) {
        var row = rows[i];
        rowHTML = ""
        for (j = 0; j < keys.length; j++) {
            key = keys[j];
            value = row[key]
            if(Array.isArray(value)){
                value = printArray(value)
            }else if(typeof value == "object"){
                value = printJson(value)
            }
            rowHTML = `
                ${rowHTML}
                <td key="${key}" >
                    ${printValue(value)}
                </td>
            `
        }
        rowsHTML = `
            ${rowsHTML}
            <tr>
                ${rowHTML}
            </tr>
        `
    }
    return rowsHTML;
}
htmlJson = (rows) => {
    var keys = Object.keys(rows[0])
    var keysHTML = ""
    for (i = 0; i < keys.length; i++) {
        keysHTML = `
            ${keysHTML}
            <th key="${keys[i]}" onclick="orderTableBy('${keys[i]}',this)" order="none">
                <span class="content">
                    ${keys[i]}
                    <span class="arrow orderby"></span>
                </span>
            </th>

        `
    }
    rowsHTML = printTbody(rows)
    rowTbodyGloabal = rows
    rowTbodyFilters = rows
    return `
        ${loadFilter(keys)}
        <table id="tablePP">
            <thead>
                <tr>
                    ${keysHTML}
                </tr>
            </thead>
            <tbody id="tbodyPP">
                ${rowsHTML}
            </tbody>
        </table>
    `
}
loadFilter = (keys) => {
    var htmlR = ""
    for (i = 0; i < keys.length; i++) {
        e = keys[i];
        inputHTML ="" 
        type = "text"
        if(e == "date"){
            type = "date"
        }
        if(keysRange.indexOf(e)>-1) {
            inputHTML = `
                <input 
                id="filter_${e}_min"
                name="filter_${e}_min" 
                class="input input_range"
                type="${type}"
                />
                <input 
                id="filter_${e}_max"
                name="filter_${e}_max" 
                class="input input_range"
                type="${type}"
                />
            `
        }else if(keysSearch.indexOf(e)>-1) {
            inputHTML = `
                <input 
                id="filter_${e}"
                name="filter_${e}" 
                class="input input_search"
                type="${type}"
                />
            `
        }else if(keysSelect.indexOf(e)>-1) {
            arrayNoRepit = rowTbodyGloabal.map((c)=>c[e])
            arrayNoRepit = ["",...arrayNoRepit.filter((item,index)=>arrayNoRepit.indexOf(item)===index)]
            console.log(arrayNoRepit);
            options = arrayNoRepit.reduce((c,n) => `${(typeof c == "string")?c:""}<option>${n}</option>`)
            inputHTML = `
                <select 
                id="filter_${e}"
                name="filter_${e}" 
                class="input input_select"
                >
                <option>Select</option>
                ${options}
                </select>
            `
        }
        if(keysNone.indexOf(e)==-1) {
            htmlR = `
                ${htmlR}
                <tr filter="${e}">
                    <th>
                        ${e}
                    </th>
                    <td>
                        ${inputHTML}
                    </td>
                    <td>
                        <button filter="${e}" onclick="filtrarTable('${e}','${type}')" class="btn_filter">
                            Filtar
                        </button>
                    </td>
                </tr>
            `       
        }
    }
    
    return `
        <input id="checkboxFilter" name="checkboxFilter" type="checkbox" hidden/>
        <table id="filters">
            <thead>
                <tr>
                    <td colspan="2">
                        <button onclick="clearFilter()" class="btn_filter">
                            Clear Filter
                        </button>
                    </td>
                </tr>
            </thead>
            <tbody>
                ${htmlR}
            </tbody>
        </table>
    `
}

writeJson = (rows , id) => {
    e = document.getElementById(id)
    if(rows.length == 0){
        e.innerHTML = `
            <h1>
                No Items
            </h1>
        `
        return;
    }
    e.innerHTML = htmlJson(rows)
}
openUser = (id) => {
    request("getCustomer",(result) => {
        e = document.getElementById('dataPanel')
        if(result.type != 'ok'){
            e.innerHTML = `
                <h1>
                    No Customer
                </h1>
            `
            return;
        }
        e.innerHTML = `
            <h1>Id ${result.customer.id}</h1>
            <table>
                <tr>
                    <th>ip</th>
                    <td>${result.customer.ipCustomer}</td>
                </tr>
                <tr>
                    <th>OS</th>
                    <td>${result.customer.OS}</td>
                </tr>
                <tr>
                    <th>Browser</th>
                    <td>${result.customer.Browser}</td>
                </tr>
                <tr>
                    <th>System</th>
                    <td>${result.customer.System}</td>
                </tr>
                <tr>
                    <th>Sessions</th>
                    <td>${result.customer.Sessions}</td>
                </tr>
                <tr>
                    <th>Events</th>
                    <td>${result.customer.Events}</td>
                </tr>
            </table>

            <label for="collapceEvent">
                <h2 class="btnC">Eventos</h2>
            </label>
            <div>
                <input type="checkbox" id="collapceEvent" name="collapceEvent" hidden />
                <div class="collapceInput">
                    ${(result.events.length == 0)?"<h3>No Items</h3>":htmlJson(result.events)}
                </div>
            </div>

            <label for="collapceOrders">
                <h2 class="btnC">Orders</h2>
            </label>
            <div>
                <input type="checkbox" id="collapceOrders" name="collapceOrders" hidden />
                <div class="collapceInput">
                    ${(result.orders.length == 0)?"<h3>No Items</h3>":htmlJson(result.orders)}
                </div>
            </div>
        `
    },{
        idCustomer : id
    })
}

loadLogin = () => {
    if (localStorage.type == "ok") return true;
    app.innerHTML = `
        <div id="contentLogin" class="contentLogin">
            <form>
                <label for="user">
                    <span>
                        User
                    </span>
                    <input
                        id="user"
                        name="user"
                        placeholder="User"
                        type="text"
                    />
                </label>
                <label for="password">
                    <span>
                        Password
                    </span>
                    <input
                        id="password"
                        name="password"
                        placeholder="Password"
                        type="password"
                    />
                </label>
                <button id="btnSubmit">
                    Login
                </button>
            </form>
        </div>
    `;

    btnSubmit   = document.getElementById('btnSubmit')
    user        = document.getElementById('user')
    password    = document.getElementById('password')
    btnSubmit.onclick = function(e){
        e.preventDefault()
        localStorage.setItem( "user" , user.value )
        localStorage.setItem( "password" , password.value )
        request("isClient",(result) => {
            localStorage.setItem( "host" , result.result.host )
            cuentas = localStorage.cuentas
            if (cuentas == undefined) {
                cuentas = "{}"
            }
            cuentas = JSON.parse(cuentas)
            cuentas[localStorage.user] = {
                "user" : localStorage.user,
                "password" : localStorage.password,
                "host" : localStorage.host,
            }
            localStorage.setItem( "cuentas" , JSON.stringify(cuentas) )
            loadPanel()
        })
    }
    return false;
}

loadData = (e) => {
    toggelLoader()
    key = e.target.getAttribute('loaddata');
    dataPanel = document.getElementById('dataPanel')
    
    switch (key) {
        case "eventos":
                request("getData",(result) => {
                    writeJson(result.result,'dataPanel')
                })
            break;
        case "usuarios":
                request("getUsers",(result) => {
                    result = result.result
                    for (i = 0; i < result.length; i++) {
                        result[i].id = `
                            <a onclick="openUser('${result[i].id}')">
                                ${result[i].id}
                            </a>
                        `
                    }
                    writeJson(result,'dataPanel')
                })
            break;
        case "orders":
                request("getOrders",(result) => {
                    writeJson(result.result,'dataPanel')
                })
            break;
        case "getProduct":
                request("getProduct",(result) => {
                    writeJson(result.result,'dataPanel')
                })
            break;
    }
    toggelLoader()
}
loadPanel = () => {
    toggelLoader()
    app.innerHTML = `
        <div id="panel">
            <div class="head">
                <select id="selectCuenta" class="btnPanel">
                </select>
                <a loadData="usuarios" class="btnPanel">
                    Usuarios
                </a>
                <a loadData="eventos" class="btnPanel">
                    Eventos
                </a>
                <a loadData="orders" class="btnPanel">
                    Orders
                </a>
                <a loadData="getProduct" class="btnPanel">
                    Productos Add
                </a>
                <label for="checkboxFilter" class="btnPanel mL">
                    Filter
                </label>
            </div>
            <div id="dataPanel" class="body"></div>
        </div>
    `
    selectCuenta = document.getElementById('selectCuenta')
    loadSelectCuenta()
    selectCuenta.onchange = (e) => {
        s = cuentas[selectCuenta.value]

        localStorage.setItem( "user" , s.user )
        localStorage.setItem( "password" , s.password )
        localStorage.setItem( "host" , s.host )

        window.location.reload()
    }
    e = document.documentElement.querySelectorAll('[loadData]')
    for (i = 0; i < e.length; i++) {
        e[i].onclick = (e) => {
            loadData(e)
        }
    }
    toggelLoader()
}
loadSelectCuenta = () => {
    cuentas = localStorage.cuentas
    if (cuentas == undefined) {
        cuentas = "{}"
        localStorage.setItem( "type" , "error" )
        window.location.reload()
        return;
    }
    cuentas = JSON.parse(cuentas)
    for (const key in cuentas) {
        selectCuenta.innerHTML+=`
            <option value="${key}" ${(key==localStorage.user)?"selected":""}>
                ${key}
            </option>
        `
    }
}
orderTableBy = (key) => {
    toggelLoader()
    tbody = document.getElementById('tbodyPP')
    e = document.querySelector(`th[key="${key}"]`)
    en = document.querySelectorAll(`th:not([key="${key}"])`)
    for (i = 0; i < en.length; i++) {
        en[i].setAttribute('order',"none")
    }


    order = e.getAttribute('order')
    rowOrderBy = rowTbodyFilters

    switch (order) {
        case "none":
            e.setAttribute('order',"top")
            rowOrderBy = rowOrderBy.sort(function (a, b) {
                if(typeof  a[key] != "string"){
                    a[key] = a[key].toString()
                    b[key] = b[key].toString()
                }
                return a[key].localeCompare(b[key]);
            })
            break;
        case "top":
            e.setAttribute('order',"down")
            rowOrderBy = rowOrderBy.sort(function (a, b) {
                if(typeof  a[key] != "string"){
                    a[key] = a[key].toString()
                    b[key] = b[key].toString()
                }
                return b[key].localeCompare(a[key]);
            })
            break;
        case "down":
            e.setAttribute('order',"none")
            break;
    }
    
    tbody.innerHTML = printTbody(rowOrderBy)
    toggelLoader()
}
filtrarTable = (e , type) => {
    toggelLoader()
    tbody = document.getElementById('tbodyPP')
    rowTbodyFilters = rowTbodyGloabal
    if(keysRange.indexOf(e)>-1){
        min = document.getElementById(`filter_${e}_min`)
        max = document.getElementById(`filter_${e}_max`)
        minimo = min.value.toLocaleLowerCase()
        maximo = max.value.toLocaleLowerCase()
        if(minimo == ""){
            alert("Ingrese Minimo")
            return;
        }
        if(maximo == ""){
            alert("Ingrese Maximo")
            return;
        }
        if(keysNumber.indexOf(e)>-1){
            minimo = parseFloat(minimo)
            maximo = parseFloat(maximo)
        }
        if(minimo > maximo){
            alert("Minimo mayor a Maximo")
            return;
        }
        rowTbodyFilters = rowTbodyFilters.filter(row => {
            value = row[e].toLocaleLowerCase()
            if(keysNumber.indexOf(e)>-1){
                value = parseFloat(value)
            }
            return  value >= minimo && value <= maximo;
        });

    }else{
        ele = document.getElementById(`filter_${e}`)
        rowTbodyFilters = rowTbodyFilters.filter(row => row[e].toLocaleLowerCase().indexOf(ele.value.toLocaleLowerCase())>-1);
    }
    var rowOrderBy = rowTbodyFilters
    if(rowOrderBy.length == 0){
        tbody.innerHTML = "<h1>No Result</h1>"
    }else{
        tbody.innerHTML = printTbody(rowOrderBy)
    }
    toggelLoader()
}
clearFilter = () => {
    toggelLoader()
    tbody = document.getElementById('tbodyPP')
    rowTbodyFilters = rowTbodyGloabal
    tbody.innerHTML = printTbody(rowTbodyFilters)
    toggelLoader()
}
toggelLoader = () => {
    document.body.classList.toggle("load")
}
window.onload = () => {
    if(loadLogin()){
        loadPanel()
    }
}