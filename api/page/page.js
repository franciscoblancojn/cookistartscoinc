var Server = {}

Server.head = () => {
    return `
        <head>
            <title>Server</title>
        </head>
    `
}


Server.footer = () => {
    return `
        <footer>
            <a href="https://startscoinc.com/" target="_blank">
                startscoinc.com
            </a>
            <a onclick="logout()">
                Logout
            </a>
        </footer>
        <script>
        logout = () => {
            localStorage.setItem( "type" , "logout" )
            window.location.reload()
        }
        </script>
    `
}

Server.content = (props = "") => {
    return `
        <html>
            ${Server.head()}
            <body class="themeDack">
                <div class="container">
                    ${props}
                </div>
                ${Server.footer()}
            </body>
        </html>
    `
}

Server.login = () => {
    return `
    <div id="contentLogin" class="contentLogin">
        <form>
            <label for="user">
                <span>
                    User
                </span>
                <input
                    id="user"
                    name="user"
                    placeholder="User"
                    type="text"
                />
            </label>
            <label for="password">
                <span>
                    Password
                </span>
                <input
                    id="password"
                    name="password"
                    placeholder="Password"
                    type="password"
                />
            </label>
            <button id="btnSubmit">
                Login
            </button>
        </form>
        <script>
            btnSubmit = document.getElementById('btnSubmit')
            user = document.getElementById('user')
            password = document.getElementById('password')
            btnSubmit.onclick = function(e){
                e.preventDefault()
                var myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/json");

                var raw = JSON.stringify({
                    "type":"isClient",
                    "user":user.value,
                    "password":password.value
                });

                var requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: raw,
                    redirect: 'follow'
                };
                var url = "https://pixeltracking.startscoinc.com/app";
                fetch(url, requestOptions)
                    .then(response => response.text())
                    .then(result => {
                        result = JSON.parse(result)
                        localStorage.setItem( "type" , result.type )
                        if(result.type == "error"){
                            alert(result.message)
                        }else{
                            localStorage.setItem( "user" , user.value )
                            localStorage.setItem( "password" , password.value )
                            localStorage.setItem( "host" , result.result.host )
                            load_data()
                        }
                    })
                    .catch(error => console.log('error', error));
            }
        </script>
        <style>
            .contentLogin{
                width:100%;
                max-width:400px;
                margin: auto;
                padding: 20px 0;
            }
            label{
                display:block;
                margin-bottom:20px;
            }
            label span{
                display:block;
                margin-bottom:5px;
                font-size:20px;
            }
            input,
            button{
                font-size:20px;
                width:100%;
                padding: 10px 15px;
                border:0;
                outline:none !important;
            }
            button{
                text-align:center;
                cursor:pointer;
                transition: .5s;
                border: 3px solid var(--color-t-1, #fff);
            }
            button:hover{
                background:var(--color-1, #202020);
                color:var(--color-t-1, #fff);
            }
        </style>
    </div>
    `
}


Server.data = () => {
    return `
        <div id="data" class="d-none">
        </div>
        <script>
            data = document.getElementById('data')
            contentLogin = document.getElementById('contentLogin')

            load_data = () => {
                if(localStorage.type != "ok")return;

                var myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/json");

                var raw = JSON.stringify({
                    "type":"getData",
                    "user":localStorage.user,
                    "password":localStorage.password,
                    "host":localStorage.host,
                });

                var requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: raw,
                    redirect: 'follow'
                };
                var url = "https://pixeltracking.startscoinc.com/app";
                fetch(url, requestOptions)
                    .then(response => response.text())
                    .then(result => {
                        result = JSON.parse(result)
                        localStorage.setItem( "type" , result.type )
                        if(result.type == "error"){
                            alert(result.message)
                            data.classList.add('d-none')
                            contentLogin.classList.remove('d-none')
                        }else{
                            data.classList.remove('d-none')
                            contentLogin.classList.add('d-none')

                            data.innerHTML = printData(result)
                        }
                    })
                    .catch(error => console.log('error', error));
            }
            load_data()

            printUtm = (utm) => {
                utm = window.atob(utm)
                utm = JSON.parse(utm)
                utmHTML = ""
                for (const key in utm) {
                    utmHTML += \`
                        <tr>
                            <td>\${key}</td>
                            <td>\${utm[key]}</td>
                        </tr>
                    \`
                }
                return \`
                    <table>
                        \${utmHTML}
                    </table>
                \`
            }
            
            printData = (data) => {
                if(data.type != "ok")return "";
                result = data.result;
                dataTable = result.reduce(function(cont, e, key, vector){
                    return \`
                        \${(typeof cont == "string")?cont:""}
                        <tr>
                            <td>\${key}</td>
                            <td>\${e.userAgent}</td>
                            <td>\${e.platform}</td>
                            <td>
                                <a href="\${e.pageUrl}" target="_blank">
                                \${e.pageUrl}
                                </a>
                            </td>
                            <td>\${e.host}</td>
                            <td>\${e.ip}</td>
                            <td>\${e.time}</td>
                            <td>\${e.date}</td>

                            <td>\${e.event}</td>
                            <td>\${e.event_target}</td>
                            <td>\${e.event_id}</td>
                            <td>\${e.event_class}</td>
                            <td>
                                <a href="\${e.event_href}" target="_blank">
                                \${e.event_href}
                                </a>
                            </td>
                            <td>\${printUtm(e.utm)}</td>
                        </tr>
                    \`;
                });
                return \`
                    <table>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>userAgent</th>
                                <th>platform</th>
                                <th>pageUrl</th>
                                <th>host</th>
                                <th>ip</th>
                                <th>time</th>
                                <th>date</th>

                                <th>event</th>
                                <th>event_target</th>
                                <th>event_id</th>
                                <th>event_class</th>
                                <th>event_href</th>

                                <th>utm</th>
                            </tr>
                        </thead>
                        <tbody>
                            \${dataTable}
                        </tbody>
                    </table>
                \`;
            }
        </script>
    `
}


Server.contentTemplate = () => {
    return `
        <div id="app"></div>
        <link rel="stylesheet" href="https://franciscoblancojn.gitlab.io/cookistartscoinc/api/page/style.css">
        <script src="https://franciscoblancojn.gitlab.io/cookistartscoinc/api/page/script.js"></script>
    `
}

Server.render = () => {
    return `
        ${Server.content(
            `
            ${Server.contentTemplate()}
            `
        )}
    `
}

module.exports = Server;