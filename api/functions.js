//var buffer = require('buffer/').Buffer;
var f = {}

f.stringObj = (result) => {
    const keys64 = [
        'utm'
    ]
    for (var key in result) {
        item = result[key]
        if (keys64.includes(key)) {
            item = (new Buffer(item, 'base64')).toString('ascii');
        }
        if (typeof item == "string") {
            if(item[0] == "{" && item[item.length -1] == "}"){
                item = JSON.parse(item)
            }else if(item[0] == "[" && item[item.length -1] == "]" && item != "[object Object]"){
                item = JSON.parse(item)
            }
        }else if(typeof item == "object"){
            item = f.stringObj(item)
        }
        result[key] = item
    }
    return result;
}

f.query = (connection,sql) =>{
    try{
        const result = f.stringObj(connection.query(sql));
        return {
            'type':'ok',
            'result':result
        };
    }catch(err){
        return {
            'type':'error',
            'error':err
        };
    }
}

f.respond = (res , responde) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(responde));
    res.flush()
    res.end();
}

f.base64 = (e) => {
    return Buffer.from(e).toString('base64')
    var buff = new Buffer(e);
    var base64data = buff.toString('base64');
    return base64data;
    return buffer.from(e).toString('base64');
}
f.getName = (a) => a.map((e)=>(typeof e == "object")?e.name:e)
f.getTitle = (a) => a.map((e)=>(typeof e == "object")?e.title:e)
f.getSelect = (json) => {
    if(!Array.isArray(json.return)){
        return {
            'type':"error",
            'message':'Return Invalid'
        };
    }
    if(json.return.length == 0){
        return {
            'type':"error",
            'message':'Return Invalid'
        };
    }
    NameReturn = f.getName(json.return)
    TitleReturn = f.getTitle(json.return)
    select = []
    for (i = 0; i < NameReturn.length; i++) {
        Name = NameReturn[i];
        Title = TitleReturn[i];
        if(Name == null || Title == null){
            return{
                'type':"error",
                'message':'Return Invalid'
            };
        }
        select.push(`${Name} AS ${Title}`)
    }
    select = select.join(",")
    return {
        'type':"ok",
        "select":select
    }
}
f.getQuery = (json) => {
    if(!Array.isArray(json.query)){
        return {
            'type':"error",
            'message':'Query Invalid'
        };
    }
    if(json.query.length == 0){
        return {
            'type':"error",
            'message':'Query Invalid'
        };
    }
    query = []
    for (i = 0; i < json.query.length; i++) {
        e = json.query[i];
        if(e == null || e.key == null || e.value == null || e.compare == null){
            return{
                'type':"error",
                'message':'Query Invalid'
            };
        }
        if(!(["<",">","=","<>",">=","<=","LIKE","BETWEEN","IN"].includes(e.compare))){
            return{
                'type':"error",
                'message':'Compare Invalid'
            };
        }
        switch (e.compare) {
            case "BETWEEN":
                if(!Array.isArray(e.value)){
                    return {
                        'type':"error",
                        'message':'Compare Invalid'
                    };
                }
                if(e.value.length != 2){
                    return {
                        'type':"error",
                        'message':'Compare Invalid'
                    };
                }
                e.value = e.value.join(" AND ")
                query.push(`${e.key} ${e.compare} ${e.value}`)
                break;
            case "IN":
                if(!Array.isArray(e.value)){
                    return {
                        'type':"error",
                        'message':'Compare Invalid'
                    };
                }
                if(e.value.length == 0){
                    return {
                        'type':"error",
                        'message':'Compare Invalid'
                    };
                }
                e.value = e.value.join("','")
                query.push(`${e.key} ${e.compare} ('${e.value}')`)
                break;
        
            default:
                query.push(`${e.key} ${e.compare} '${e.value}'`)
                break;
        }
    }
    query = query.join(" AND ")
    return {
        'type':"ok",
        "query":query
    }
}
module.exports = f;