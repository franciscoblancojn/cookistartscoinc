//functions
const f = require("./functions")
var geoip = require('geoip-lite');
var moment = require("moment-timezone");


//obj db 
var db = {}
const sql = {
    getHosts : "SELECT host FROM clientes",
    getUser : (json) => {
        return `SELECT user FROM clientes WHERE user = '${json.user}' AND password = '${json.password}' AND host = '${json.host}'`;
    },
    getUserByUserPassword: (json) => {
        return `SELECT user,host FROM clientes WHERE user = '${json.user}' AND password = '${json.password}'`;
    },
    getData: (json , select = "*", query = "") => {
        return `SELECT ${select} FROM data WHERE host = '${json.host}' ${(query=="")?"":"AND"} ${query} ORDER BY id DESC`;
    },
    getEvent: (json , select = "*") => {
        return `SELECT ${select} FROM data WHERE id = '${json.id}' ORDER BY id DESC`;
    },
    getDataByIp: (json) => {
        return `SELECT * FROM data WHERE host = '${json.host}' AND ip = '${json.ip}' ORDER BY id DESC`;
    },
    getProductsByIp: (json) => {
        return `SELECT * FROM addProduct WHERE host = '${json.host}' AND ip = '${json.ip}' ORDER BY id DESC`;
    },
    saveData : (json) => {
        const keys = [
            `userAgent`  ,
            `platform` ,
            `pageUrl` ,
            `host` ,
            `ip` ,
            `time` ,
            `date` ,
            `event` ,
            `event_target` ,
            `event_id` ,
            `event_class` ,
            `event_href` ,
            `utm` ,
        ]
        const keys64 = [
            'utm'
        ]
        var values = [], v = "";
        for (var i = 0; i < keys.length; i++) {
            v = json[keys[i]];
            if(keys64.includes(keys[i])){
                //v = Buffer.from(e).toString('base64')
            }
            if(typeof v == "object"){
                v = JSON.stringify(v)
            }
            values[i] = v
        }
        return `INSERT INTO data (${keys.join(',')}) VALUES ('${values.join("','")}');`;
    },
    addProduct : (json , keys) => {
        var values = [], v = "";
        for (var i = 0; i < keys.length; i++) {
            v = json[keys[i]];
            values[i] = v
        }
        return `INSERT INTO addProduct (${keys.join(',')}) VALUES ('${values.join("','")}');`;
    },
    getProduct : (json , select = "*", query = "") => {
        return `SELECT ${select} FROM addProduct WHERE host = '${json.host}' ${(query=="")?"":"AND"} ${query} ORDER BY id DESC`;
    },
    getSingleProduct : (json , select = "*") => {
        return `SELECT ${select} FROM addProduct WHERE host = '${json.host}' AND id = '${json.id}' ORDER BY id DESC`;
    },
    addUser : (json , keys) => {
        var values = [], v = "";
        for (var i = 0; i < keys.length; i++) {
            v = json[keys[i]];
            values[i] = v
        }
        return `INSERT INTO customer (${keys.join(',')}) VALUES ('${values.join("','")}');`;
    },
    getUsers : (json , where = "") => {
        return `
            SELECT 
            TO_BASE64(CONCAT("${json.host}_",id,"_",ip)) AS id,
            ip as ipCustomer,
            CASE 
                WHEN userAgent LIKE '%Mac%OS%' THEN 'Mac OS X'
                WHEN userAgent LIKE '%iPad%' THEN 'iPad'
                WHEN userAgent LIKE '%iPod%' THEN 'iPod'
                WHEN userAgent LIKE '%iPhone%' THEN 'iPhone'
                WHEN userAgent LIKE '%imac%' THEN 'mac'
                WHEN userAgent LIKE '%android%' THEN 'android'
                WHEN userAgent LIKE '%linux%' THEN 'linux'
                WHEN userAgent LIKE '%Nokia%' THEN 'Nokia'
                WHEN userAgent LIKE '%BlackBerry%' THEN 'BlackBerry'
                WHEN userAgent LIKE '%win%' THEN
                    CASE
                        WHEN userAgent LIKE '%NT 6.2%' THEN 'Windows 8'
                        WHEN userAgent LIKE '%NT 6.3%' THEN 'Windows 8.1'
                        WHEN userAgent LIKE '%NT 6.1%' THEN 'Windows 7'
                        WHEN userAgent LIKE '%NT 6.0%' THEN 'Windows Vista'
                        WHEN userAgent LIKE '%NT 5.1%' THEN 'Windows XP'
                        WHEN userAgent LIKE '%NT 5.0%' THEN 'Windows 2000'
                        ELSE 'Windows'
                    END      
                WHEN userAgent LIKE '%FreeBSD%' THEN 'FreeBSD'
                WHEN userAgent LIKE '%OpenBSD%' THEN 'OpenBSD'
                WHEN userAgent LIKE '%NetBSD%' THEN 'NetBSD'
                WHEN userAgent LIKE '%OpenSolaris%' THEN 'OpenSolaris'
                WHEN userAgent LIKE '%SunOS%' THEN 'SunOS'
                WHEN userAgent LIKE '%OS/2%' THEN 'OS/2'
                WHEN userAgent LIKE '%BeOS%' THEN 'BeOS'
                ELSE 'Unknown'
            END AS "OS",
            (
            SELECT 
            GROUP_CONCAT(DISTINCT Browser SEPARATOR '<hr>') AS "Browser"
            FROM
            (
                SELECT 
                ip ,
                CASE
                    WHEN userAgent LIKE '%edge%'THEN 'Edge'
                    WHEN userAgent LIKE '%MSIE%' THEN 'Internet Explorer'
                    WHEN userAgent LIKE '%Firefox%' THEN 'Mozilla Firefox'
                    WHEN userAgent LIKE '%Chrome%' THEN 'Google Chrome'
                    WHEN userAgent LIKE '%Safari%' THEN 'Apple Safari'
                    WHEN userAgent LIKE '%Opera%' THEN 'Opera' 
                    WHEN userAgent LIKE '%Outlook%' THEN 'Outlook' 
                    ELSE 'Unknown'
                END AS "Browser"
                FROM data
                WHERE data.ip = ipCustomer
                )AS DATASS
            )AS "Browser",
            CASE
                WHEN userAgent LIKE '%WOW64%' THEN '64 bit'
                WHEN userAgent LIKE '%x64%' THEN '64 bit'
                ELSE '32 bit'
            END AS "System",
            (
                SELECT COUNT(*) FROM data WHERE ip = ipCustomer AND event = "load" AND host = '${json.host}'
            ) AS "Sessions" ,
            (
                SELECT COUNT(*) FROM data WHERE ip = ipCustomer AND host = '${json.host}'
            ) AS "Events" ,
            (
                SELECT COUNT(*) FROM compras WHERE ip = ipCustomer AND host = '${json.host}' AND (state = 'processing' OR state = 'completed')
            ) AS "ventas" 
            FROM customer
            ${where}
            HAVING sessions > 0
            ;
        `;
    },
    getCustomer : (json) => {
        return sql.getUsers(json , `WHERE id="${json.idCustomer}"`)
    },
    addOrder : (json , keys) => {
        const keysObj = [
            'products'
        ]
        var values = [], v = "";
        for (var i = 0; i < keys.length; i++) {
            v = json[keys[i]];
            if(keysObj.includes(keys[i])){
                v = JSON.stringify(v)
            }
            values[i] = v
        }
        return `INSERT INTO compras (${keys.join(',')}) VALUES ('${values.join("','")}');`;
    },
    getOrders : (json , select = "*", query = "") => {
        return `SELECT ${select} FROM compras WHERE host = '${json.host}' ${(query=="")?"":"AND"} ${query} ORDER BY id DESC`;
    },
    getOrder : (json, select = "*") => {
        return `SELECT ${select} FROM compras WHERE host = '${json.host}' AND id = '${json.id}' ORDER BY id DESC`;
    },
    getOrdersByIp : (json) => {
        return `SELECT * FROM compras WHERE host = '${json.host}' AND ip = '${json.ip}' ORDER BY id DESC`;
    },
    getUserActive : (json) => {
        const hora = moment().tz("America/Bogota").format('h');
        const minutos = moment().tz("America/Bogota").format('m');
        const dateDMY = moment().tz("America/Bogota").format('D/M/YYYY')
        const dateTime = `${(minutos - 10 > 0)?hora:(hora-1)}:${(minutos - 10 > 0)?(minutos - 10):(minutos - 10 + 60)}`
        return ` SELECT COUNT(*) AS userActives 
        FROM data WHERE host = '${json.host}' AND event = 'load' AND date = '${dateDMY}' AND time >= '${dateTime}' ORDER BY id DESC`
    }
}
//mysql
const sync_mysql = require('sync-mysql');
//db.connection
db.loadConnection = (config) =>{
    db.connection = new sync_mysql(config);
}
//db.sql
db.sql = (sql) => {
    return f.query(db.connection,sql)
}
//db.validateStringUserPassword
db.validateStringUserPassword = (json) =>{
    if(json.user == null || json.user == undefined || json.user == "")
        return{
            'type':"error",
            'message':'json.user undefined'
        };
    if(json.user.replace(/[|+=*&^%$#@!~\-(){}\[\];:'"/\\><]/g,'')!=json.user)
        return{
            'type':"error",
            'message':'json.user invalid error sintaxis'
        };
    if(json.password == null || json.password == undefined || json.password == "")
        return{
            'type':"error",
            'message':'json.password undefined'
        };
    if(json.password.replace(/[|+=*&^%$#@!~\-(){}\[\];:'"/\\><]/g,'')!=json.password)
        return{
            'type':"error",
            'message':'json.password invalid error sintaxis'
        };
    return {
        "type":"ok"
    }
}
//db.validateUserPassword
db.validateUserPassword = (json) => {
    var r;
    r = db.validateStringUserPassword(json)
    if(r.type == 'error')return r;
    
    r = db.sql(sql.getUser(json))
    if(r.type == 'error')return r;
    
    if(r.result.length == 0)
        return{
            'type':"error",
            'message':'User or Password invalid'
        };
    return{
        'type':"ok",
    };
}
//db.validateHost
db.validateHost = (json) => {
    if(json.host == null || json.host == undefined || json.host == "")
        return{
            'type':"error",
            'message':'json.host undefined'
        };
    if(typeof json.host != "string")
        return{
            'type':"error",
            'message':'json.host not string'
        };
    if(json.host.replace(/[|+=*&^%$#@!~\-(){}\[\];:'"/\\><]/g,'')!=json.host)
        return{
            'type':"error",
            'message':'json.host invalid error sintaxis'
        };
    //validate in database
    var r = db.sql(sql.getHosts);
    if(r.type == 'error') return r;

    var hosts = r.result.map((e,key)=>{return e.host});
    if(!hosts.includes(json.host))
        return{
            'type':"error",
            'message':'json.host invalid not found'
        };
    
    return{
        'type':"ok",
        'message':'',
    };
}
//db.validateData 
db.validateData = (json , keysP = [
        `userAgent`,
        `platform`,
        `pageUrl`,
        `host`,
        `ip`,
        `time`,
        `date`,
        `event`,
    ]) => {
    for (let i = 0; i < keysP.length; i++) {
        if (json[keysP[i]] == null || json[keysP[i]] == undefined) {
            return {
                "type":"error",
                "message":`json.${keysP[i]} undefined`,
            };
        }
    }
    return{
        'type':"ok",
        'message':'',
    };
}
//db.addUser
db.addUser = (json) => {
    const keys = [
        `ip` ,
        `userAgent`,
        `host`,
    ]
    f.query(db.connection,sql.addUser(json,keys))
}
//db.saveData
db.saveData = (json) => {
    var respond;
    respond = db.validateHost(json)
    if(respond.type == 'error')return respond;
    respond = db.validateData(json)
    if(respond.type == 'error')return respond;


    respond.sql = sql.saveData(json)

    db.addUser(json)

    return f.query(db.connection,respond.sql)
}
//db.getData Return
db.getData = (json) => {
    
    respond = db.validateHost(json)
    if(respond.type == 'error')return respond;

    respond = db.validateUserPassword(json)
    if(respond.type == 'error')return respond;
    
    var select = "*"
    if(json.return != undefined){
        respond = f.getSelect(json)
        if(respond.type == 'error')return respond;
        select = respond.select
    }
    var query = ""
    if(json.query != undefined){
        respond = f.getQuery(json)
        if(respond.type == 'error')return respond;
        query = respond.query
    }
    r = f.query(db.connection,sql.getData(json,select,query))
    
    return r;
}
//db.isClient
db.isClient = (json) => {
    var r;
    r = db.validateStringUserPassword(json)
    if(r.type == 'error')return r;
    
    r = f.query(db.connection,sql.getUserByUserPassword(json));
    if(r.result.length == 0)
        return{
            'type':"error",
            'message':'User or Password invalid'
        };
    return{
        'type':"ok",
        'result':r.result[0]
    };
}
//db.users
db.getUsers = (json) => {
    var respond;
    respond = db.validateHost(json)
    if(respond.type == 'error')return respond;

    respond = db.validateUserPassword(json)
    if(respond.type == 'error')return respond;
    const sqlL = sql.getUsers(json)
    respond = f.query(db.connection,sqlL)

    respond.result = respond.result.map((e)=>{
        var geo = geoip.lookup(e.ipCustomer)
        
        if(geo){
            e.pais = geo.country
            e.ciudad = geo.city
        }else{
            e.pais = ""
            e.ciudad = ""
        }
        return {
            "id": e.id,
            "Marca":"falta",
            "ipCustomer": e.ipCustomer,
            "Pais": e.pais,
            "Ciudad": e.ciudad,
            "OS": e.OS,
            "System": e.System,
            "Browser": e.Browser,
            "Sessions": e.Sessions,
            "Events": e.Events,
            "Ventas": e.ventas
        }
    })
    return respond;
}
//db.cutomers
db.getCustomer = (json) => {
    respond = db.validateHost(json)
    if(respond.type == 'error')return respond;

    respond = db.validateUserPassword(json)
    if(respond.type == 'error')return respond;


    const  keysValidate = [
        `idCustomer` ,
    ]
    respond = db.validateData(json ,keysValidate)
    if(respond.type == 'error')return respond;

    json.idCustomer = (new Buffer(json.idCustomer, 'base64')).toString('ascii');
    aux = json.idCustomer.split("_")
    json.idCustomer = aux[1]
    json.ip = aux[2]

    respond = f.query(db.connection,sql.getCustomer(json));
    if(respond.type == 'error')return respond;

    r ={}
    r.type = "ok"

    r.customer = respond.result[0]

    respond = f.query(db.connection,sql.getDataByIp(json));
    if(respond.type == 'error')return respond;

    r.events = respond.result


    respond = f.query(db.connection,sql.getOrdersByIp(json));
    if(respond.type == 'error')return respond;

    r.orders = respond.result

    json.ip = r.customer.ipCustomer
    respond = f.query(db.connection,sql.getProductsByIp(json));
    if(respond.type == 'error')return respond;

    r.addToCart = respond.result

    return r;
    return f.query(db.connection,sql.getCustomer(json));
}
//db.user
db.addOrder = (json) => {
    respond = db.validateHost(json)
    if(respond.type == 'error')return respond;

    const  keysValidate = [
        `host` ,
        `ip`,
        `order_id` ,
        `state`,
        `url` ,
        `payment` ,
        `date` ,
        `total` ,
        `customer_id` ,
        `billing_first_name` ,
        `billing_last_name` ,
        `billing_address_1` ,
        `billing_address_2` ,
        `billing_city` ,
        `billing_state` ,
        `billing_postcode` ,
        `billing_country` ,
        `billing_email` ,
        `billing_phone` ,
        `products` ,
    ]

    respond = db.validateData(json ,keysValidate)
    if(respond.type == 'error')return respond;
    
    return f.query(db.connection,sql.addOrder(json,keysValidate));
}
//db.getOrders Return
db.getOrders = (json) => {
    respond = db.validateHost(json)
    if(respond.type == 'error')return respond;

    respond = db.validateUserPassword(json)
    if(respond.type == 'error')return respond;

    
    var select = "*"
    if(json.return != undefined){
        respond = f.getSelect(json)
        if(respond.type == 'error')return respond;
        select = respond.select
    }


    var query = ""
    if(json.query != undefined){
        respond = f.getQuery(json)
        if(respond.type == 'error')return respond;
        query = respond.query
    }
    r = f.query(db.connection,sql.getOrders(json,select,query))
    return r;
}
//db.getOrder Return
db.getOrder = (json) => {
    respond = db.validateHost(json)
    if(respond.type == 'error')return respond;

    respond = db.validateUserPassword(json)
    if(respond.type == 'error')return respond;

    const  keysValidate = [
        `id` ,
    ]
    respond = db.validateData(json ,keysValidate)
    if(respond.type == 'error')return respond;

    var select = "*"
    if(json.return != undefined){
        respond = f.getSelect(json)
        if(respond.type == 'error')return respond;
        select = respond.select
    }
    respond = f.query(db.connection,sql.getOrder(json,select))

    respond.result = respond.result[0]

    return respond;
}
//db.addProduct
db.addProduct = (json) => {
    respond = db.validateHost(json)
    if(respond.type == 'error')return respond;
    const  keysValidate = [
        `host`,
        `ip` ,
        `product_id` ,
        `date` ,
        `name` ,
        `product_type`,
        `quantity`,
        `variation_id`,
        `price`,
        `sku`,
        `permalink` ,
        `weight`,
        `length`,
        `width`,
        `height`,
    ]

    respond = db.validateData(json ,keysValidate)
    if(respond.type == 'error')return respond;
    
    return f.query(db.connection,sql.addProduct(json,keysValidate));

}
//db.getProduct Return
db.getProduct = (json) => {
    respond = db.validateHost(json)
    if(respond.type == 'error')return respond;

    respond = db.validateUserPassword(json)
    if(respond.type == 'error')return respond;

    var select = "*"
    if(json.return != undefined){
        respond = f.getSelect(json)
        if(respond.type == 'error')return respond;
        select = respond.select
    }

    var query = ""
    if(json.query != undefined){
        respond = f.getQuery(json)
        if(respond.type == 'error')return respond;
        query = respond.query
    }
    r = f.query(db.connection,sql.getProduct(json,select,query))
    
    return r;
}
//db.getSingleProduct Return
db.getSingleProduct = (json) => {
    respond = db.validateHost(json)
    if(respond.type == 'error')return respond;

    respond = db.validateUserPassword(json)
    if(respond.type == 'error')return respond;

    const  keysValidate = [
        `id` ,
    ]
    respond = db.validateData(json ,keysValidate)
    if(respond.type == 'error')return respond;

    var select = "*"
    if(json.return != undefined){
        respond = f.getSelect(json)
        if(respond.type == 'error')return respond;
        select = respond.select
    }

    respond = f.query(db.connection,sql.getSingleProduct(json,select))

    respond.result = respond.result[0]

    return respond;
}
//db.getEvent Return
db.getEvent = (json) => {
    respond = db.validateHost(json)
    if(respond.type == 'error')return respond;

    respond = db.validateUserPassword(json)
    if(respond.type == 'error')return respond;

    const  keysValidate = [
        `id` ,
    ]
    respond = db.validateData(json ,keysValidate)
    if(respond.type == 'error')return respond;

    var select = "*"
    if(json.return != undefined){
        respond = f.getSelect(json)
        if(respond.type == 'error')return respond;
        select = respond.select
    }

    respond = f.query(db.connection,sql.getEvent(json,select))

    respond.result = respond.result[0]

    return respond;
}
db.getUserActive = (json) =>{
    respond = db.validateHost(json)
    if(respond.type == 'error')return respond;

    respond = db.validateUserPassword(json)
    if(respond.type == 'error')return respond;

    const sqlL = sql.getUserActive(json)
    respond = f.query(db.connection,sqlL)
    if(respond.type == 'error')return respond;

    respond.result = respond.result[0]
    respond.sqlL =sqlL
    return respond
}

module.exports = db;