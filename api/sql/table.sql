DROP TABLE IF EXISTS `data`;
CREATE TABLE `data` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `userAgent` varchar(500) NOT NULL ,
  `platform` varchar(500) NOT NULL,
  `pageUrl` varchar(500) NOT NULL,
  `host` varchar(500) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `time` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `event` varchar(100) NOT NULL,
  `event_target` varchar(500),
  `event_id` varchar(500),
  `event_class` varchar(500),
  `event_href` varchar(500),
  `utm` varchar(1000),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(100) UNIQUE NOT NULL ,
  `password` varchar(500) NOT NULL,
  `host` varchar(500) UNIQUE NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `compras`;
CREATE TABLE `compras` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `host` varchar(500) NOT NULL,
  `ip` varchar(500) NOT NULL,
  `order_id` varchar(10) NOT NULL ,
  `state` varchar(10) NOT NULL ,
  `url` varchar(500) NOT NULL,
  `payment` varchar(500) NOT NULL,
  `date` varchar(500) NOT NULL,
  `total` varchar(500) NOT NULL,
  `customer_id` varchar(500) NOT NULL,
  `billing_first_name` varchar(500) NOT NULL,
  `billing_last_name` varchar(500) NOT NULL,
  `billing_address_1` varchar(500) NOT NULL,
  `billing_address_2` varchar(500) NOT NULL,
  `billing_city` varchar(500) NOT NULL,
  `billing_state` varchar(500) NOT NULL,
  `billing_postcode` varchar(500) NOT NULL,
  `billing_country` varchar(500) NOT NULL,
  `billing_email` varchar(500) NOT NULL,
  `billing_phone` varchar(500) NOT NULL,
  `products` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(100) UNIQUE NOT NULL ,
  `userAgent` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `addProduct`;
CREATE TABLE `addProduct` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `host` varchar(500) NOT NULL,
  `ip` varchar(500) NOT NULL,
  `product_id` varchar(10) NOT NULL ,
  `date` varchar(50) NOT NULL ,
  `name` varchar(50) NOT NULL ,
  `product_type` varchar(50) NOT NULL,
  `quantity` varchar(10) NOT NULL,
  `variation_id` varchar(10) NOT NULL,
  `price` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `permalink` varchar(500) NOT NULL,
  `weight` varchar(50) NOT NULL,
  `length` varchar(50) NOT NULL,
  `width` varchar(50) NOT NULL,
  `height` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

