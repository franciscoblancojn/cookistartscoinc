//requires
const express = require('express'),
      bodyParser = require('body-parser'),
      f = require("./functions");

const Server = require('./page/page.js');


require("dotenv").config();

//const
const port = 3003;
const rute = '/server';


//APP
var app = express();
app.set('port', port);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());
app.use(express.urlencoded({extended: false}));

//DATABASE
// var db = require("./db")
// const config = {
//     host: "localhost",
//     user: process.env.DB_user,
//     password: process.env.DB_password,
//     database : process.env.DB_database,
// }
// db.loadConnection(config)


app.get(rute, function(req, res) {
    res.setHeader('Content-Type', 'text/html');
    res.send(Server.render());
    res.flush()
    res.end();
});
app.post(rute, function(req, res) {
    var responde = {
        "type":"post"
    }
    f.respond(res , responde)
});
app.put(rute, function(req, res) {
    var responde = {
        "type":"put"
    }
    f.respond(res , responde)
});
app.delete(rute, function(req, res) {
    var responde = {
        "type":"delete"
    }
    f.respond(res , responde)
});

app.listen(port, function() {
    console.log('ok');
});