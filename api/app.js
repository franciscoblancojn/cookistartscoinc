//requires
const express = require('express'),
      bodyParser = require('body-parser'),
      f = require("./functions");


require("dotenv").config();

//const
const port = 3002;
const rute = '/app';


//APP
var app = express();
app.set('port', port);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());
app.use(express.urlencoded({extended: false}));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});


//DATABASE
var db = require("./db")
const config = {
    host: "localhost",
    user: process.env.DB_user,
    password: process.env.DB_password,
    database : process.env.DB_database,
}
db.loadConnection(config)
app.get(rute, function(req, res) {
    var json = req.body;
    var responde;

    if(json.type == 'isClient'){
        responde = db.isClient(json)
    }else{
        responde = db.getData(json)
    }

    f.respond(res , responde)
});
app.post(rute, function(req, res) {
    var json = req.body;
    var responde;
    switch (json.type) {
        case 'isClient':
            responde = db.isClient(json)
            break;
        case 'getData':
            responde = db.getData(json)
            break;
        case 'getEvent':
            responde = db.getEvent(json)
            break;
        case 'getUsers':
            responde = db.getUsers(json)
            break;
        case 'getCustomer':
            responde = db.getCustomer(json)
            break;
        case 'addOrder':
            responde = db.addOrder(json)
            break;
        case 'getOrders':
            responde = db.getOrders(json)
            break;
        case 'getOrder':
            responde = db.getOrder(json)
            break;
        case 'addProduct':
            responde = db.addProduct(json)
            break;
        case 'getProduct':
            responde = db.getProduct(json)
            break;
        case 'getSingleProduct':
            responde = db.getSingleProduct(json)
            break;
        case 'getUserActive':
            responde = db.getUserActive(json)
            break;
        default:
            responde = db.saveData(json)
            break;
    }

    f.respond(res , responde)
});
app.put(rute, function(req, res) {
    var responde = {
        "type":"put"
    }
    f.respond(res , responde)
});
app.delete(rute, function(req, res) {
    var responde = {
        "type":"delete"
    }
    f.respond(res , responde)
});

app.listen(port, function() {
    console.log('ok');
});