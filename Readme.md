<h1>
CookiStartscoinc / Pixeltracking
</h1>

<h2>Api</h2>
<p>
    Api de Startscoinc donde se procesan los Datos
    <br>
    <a href="https://pixeltracking.startscoinc.com" target="_blank">https://pixeltracking.startscoinc.com</a>
</p>

<h2>Plugin</h2>
<p>
    Plugin que envia la informacion al Api
</p>
<br>

<h2>Page</h2>
<p>
    Page que muestra esa informacion
    <br>
    <a href="https://pixeltracking.vercel.app" target="_blank">https://pixeltracking.vercel.app</a>
</p>