<?php
/**
 * COST_script hook en wp_footer
 * 
 * @access public
 * @return void
 */
function COST_script()
{
    ?>
    <script>
        COST_script = {
            "type"  : "COST_script",
            "api"   : "<?=COST_location_url?>action.php",
            "get"   : "<?=base64_encode(json_encode($_GET))?>",
            "ip"    : "<?=COST_getIpCliente()?>",
        }
    </script>
    <script src="<?=COST_location_url?>js/api.js"></script>
    <?php
}
add_action( 'wp_footer', 'COST_script', 10 );



/**
 * COST_order_send envia informacion de la orden al api
 * 
 * @access public
 * @return void
 */
function COST_order_send($order_id)
{
    $order = wc_get_order( $order_id );

    $products = array();
    foreach ( $order->get_items() as $item_id => $item ) {
        $product_id = $item->get_product_id();
        $variation_id = $item->get_variation_id();
        $product = $item->get_product();
        $name = $item->get_name();
        $quantity = $item->get_quantity();
        $subtotal = $item->get_subtotal();
        $total = $item->get_total();
        $tax = $item->get_subtotal_tax();
        $taxclass = $item->get_tax_class();
        $taxstat = $item->get_tax_status();
        $allmeta = $item->get_meta_data();
        $somemeta = $item->get_meta( '_whatever', true );
        $type = $item->get_type();

        $product = wc_get_product( $product_id );

        $products[] =  array(
            "id"            => $product_id,
            "name"          => $name,
            "quantity"      => $quantity,
            "variation_id"  => $variation_id,
            "subtotal"      => $$subtotal,
            "total"         => $total,
            "sku"           => $product->get_sku(),
            "permalink"     => get_permalink( $product->get_id() ),

            "weight"        => $product->get_weight(),
            "length"        => $product->get_length(),
            "width"         => $product->get_width(),
            "height"        => $product->get_height(),
        );
    }
    $json = array(
        "type"                  => "addOrder",
        "host"                  => $_SERVER['HTTP_HOST'],
        "ip"                    => COST_getIpCliente(),
        "order_id"              => $order_id,
        "state"                 => $order->get_status(),
        "url"                   => $order->get_checkout_order_received_url(),
        "payment"               => $order->get_payment_method_title(),
        "date"                  => date('c'),
        "total"                 => $order->get_total(),
        "customer_id"           => $order->get_customer_id(),
        "billing_first_name"    => $order->get_billing_first_name(),
        "billing_last_name"     => $order->get_billing_last_name(),
        "billing_address_1"     => $order->get_billing_address_1(),
        "billing_address_2"     => $order->get_billing_address_2(),
        "billing_city"          => $order->get_billing_city(),
        "billing_state"         => $order->get_billing_state(),
        "billing_postcode"      => $order->get_billing_postcode(),
        "billing_country"       => $order->get_billing_country(),
        "billing_email"         => $order->get_billing_email(),
        "billing_phone"         => $order->get_billing_phone(),
        "products"              => $products
    );

    $api = new COST_api();

    $r = $api->request($json);

    update_post_meta($order_id,"sendStartscoin",json_encode($json));
    update_post_meta($order_id,"respondStartscoin",$r);
}


/**
 * COST_order_status_changed hook de status change ordenes
 * 
 * @access public
 * @return void
 */
function COST_order_status_changed($order_id) {
    COST_order_send($order_id);
}
add_action('woocommerce_order_status_changed',   'COST_order_status_changed' , 10, 1); 


/**
 * COST_add_cart hook de status change cart
 * 
 * @access public
 * @return void
 */
function COST_add_cart( $cartItemData ) {
    global $woocommerce;
    $items = $woocommerce->cart->get_cart();
    $product = [];
    foreach($items as $item => $values) { 
        $_product =  wc_get_product( $values['product_id']); 

        $product[] = array(
            "id"        => $values['product_id'],
            "name"      => $_product->get_title(),
            "quantity"  => $values['quantity'],
            "price"     => get_post_meta($values['product_id'] , '_price', true)
        );
    }
    $json = array(
        "type"                  => "addProduct",
        "host"                  => $_SERVER['HTTP_HOST'],
        "ip"                    => COST_getIpCliente(),
        "products"              => $product
    );
    $api = new COST_api();

    //$r = $api->request($json);
	return $cartItemData;
}
//add_filter( 'woocommerce_add_cart_item_data', 'COST_add_cart', 10, 1 );
function custom_action_add_to_cart($cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data)
{

    $product = wc_get_product( $product_id );
    $json = array(
        "type"                  => "addProduct",
        "host"                  => $_SERVER['HTTP_HOST'],
        "ip"                    => COST_getIpCliente(),
        "product_id"            => $product_id,
        "date"                  => date("c"),
        "name"                  => $product->get_name(),
        "product_type"          => $product->get_type(),
        "quantity"              => $quantity,
        "variation_id"          => $variation_id,
        "price"                 => $product->get_price(),
        "sku"                   => $product->get_sku(),
        "permalink"             => get_permalink( $product->get_id() ),
        "weight"                => $product->get_weight(),
        "length"                => $product->get_length(),
        "width"                 => $product->get_width(),
        "height"                => $product->get_height(),
    );

    $api = new COST_api();

    $r = $api->request($json);
}

add_action('woocommerce_add_to_cart', 'custom_action_add_to_cart', 20, 6);