<?php
/*
Plugin Name: CookiStartscoinc
Plugin URI: https://startscoinc.com/es/
Description: Plugin para CookiStartscoinc
Author: Startsco
Version: 0.1
Author URI: https://startscoinc.com/es/#
License: 
*/
/**
 * Require admin
 * Is field for requiere fields
 */
define("prefix_COST","COST");
define("COST_location_path",plugin_dir_path( __FILE__ ));
define("COST_location_url",plugin_dir_url( __FILE__ ));
define("COST_api","https://pixeltracking.startscoinc.com/app");


require_once plugin_dir_path( __FILE__ ) . 'functions.php';
require_once plugin_dir_path( __FILE__ ) . 'api.php';
require_once plugin_dir_path( __FILE__ ) . 'hook.php';